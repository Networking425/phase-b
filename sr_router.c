/**
 *  Simple Router - Phase 1
 *
 *  @author: Hercy (Yunhao Zhang)
 *  @author: Jeremy Mark Norris
 */

/**********************************************************************
 * file:  sr_router.c 
 * date:  Mon Feb 18 12:50:42 PST 2002  
 * Contact: casado@stanford.edu 
 *
 * Description:
 * 
 * This file contains all the functions that interact directly
 * with the routing table, as well as the main entry method
 * for routing. 11
 * 90904102
 **********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <time.h>


#include "sr_if.h"
#include "sr_rt.h"
#include "sr_router.h"
#include "sr_protocol.h"



#define DEBUG       1



#define MAC_SIZE ETHER_ADDR_LEN * sizeof(unsigned char)


#define SESSION_TIME    15
#define NAME_LENGTH     10

#define TIME_OUT        -2
#define INVALID         -1
#define VALID           1


#define NOT_FOUND       0

#define FOUND           1


#define TCP             6
#define SUBMASK         0x0000

static int i;// j;
static u_short cksum(u_short * buf, int count);






static int get_time();

/*---------------------------------------------------------------------
 * The ARP_Packet linkedlist handles ARP
 *---------------------------------------------------------------------*/
static void print_list();
static int list_find(uint32_t ip);
static unsigned char mac_found[MAC_SIZE];
static void list_insert_front(uint32_t ip, unsigned char * mac, struct sr_instance *sr);
//static void list_insert_rear(uint32_t ip, unsigned char * mac, struct sr_instance *sr);

typedef struct ARP_Packet
{
    // TODO: There's gonna be some data entries

    int id;
    int time_received;
    struct sr_instance* sr;
    int ref;
    
    char type[NAME_LENGTH];
    
    
    //unsigned short  ar_hrd;             /* format of hardware address   */
    //unsigned short  ar_pro;             /* format of protocol address   */
    //unsigned char   ar_hln; //3.        /* length of hardware address   */
    //unsigned char   ar_pln; //4.        /* length of protocol address   */
    //unsigned short  ar_op;  //5.        /* ARP opcode (command)         */
    unsigned char   ar_sha[ETHER_ADDR_LEN];   /* source sender hardware address      */
    uint32_t        ar_sip;             /* sender IP address            */
    unsigned char   ar_tha[ETHER_ADDR_LEN];   /* target hardware address      */
    uint32_t        ar_tip;
    unsigned char   ar_mac[ETHER_ADDR_LEN];
    uint32_t        ar_ip;
    
    struct ARP_Packet * next;

} ARP_Packet;

ARP_Packet * ARP_Cache;


static int ck_destination_ip(struct sr_if * iface_list, uint32_t ip);

/*--------------------------------------------------------------------- 
 * Method: sr_init(void)
 * Scope:  Global
 *
 * Initialize the routing subsystem
 * 
 *---------------------------------------------------------------------*/
void sr_init(struct sr_instance* sr) 
{
    if(DEBUG)
    {
        printf("\n*** @ sr_init: Started\n");
        printf("    > Initializing... ");
    }

    /* REQUIRES */
    assert(sr);
    
    /* Add initialization code here! */
    ARP_Cache = malloc(sizeof(ARP_Packet));
    
    if(DEBUG)
    {
        printf("OK\n");
    }
} /* -- sr_init -- */



/*---------------------------------------------------------------------
 * Method: sr_handlepacket(uint8_t* p,char* interface)
 * Scope:  Global
 *
 * This method is called each time the router receives a packet on the
 * interface.  The packet buffer, the packet length and the receiving
 * interface are passed in as parameters. The packet is complete with
 * ethernet headers.
 *
 * Note: Both the packet buffer and the character's memory are handled
 * by sr_vns_comm.c that means do NOT delete either.  Make a copy of the
 * packet instead if you intend to keep it around beyond the scope of
 * the method call.
 *
 *---------------------------------------------------------------------*/
void sr_handlepacket(struct sr_instance * sr, 
        uint8_t * packet/* lent */,
        unsigned int len,
        char * interface/* lent */)
{
    if(DEBUG)
    {
        printf("\n*** @ sr_handlepacket: Started\n");
    }

    
    /* REQUIRES */
    assert(sr);
    assert(packet);
    assert(interface);

    if(DEBUG)
    {
        printf("    > Received packet of length: %d \n", len);
        printf("    > Interface: %s\n", interface);
        
        printf("    > Username: %s\n", sr->user);
        printf("    > Hostname: %s\n", sr->host);
        printf("    > Template: %s\n", sr->template);
        printf("    > auth_key_fn: %s\n", sr->auth_key_fn);
        printf("    > topo_id: %d\n", sr->topo_id);
    }

    
    // Get all headers
    struct sr_if * iface = sr_get_interface(sr, interface);
    struct sr_ethernet_hdr * e_hdr = (struct sr_ethernet_hdr *)packet;
    
    //printf("    - ether_type: %d\n", ntohs(e_hdr->ether_type));
    
    if(DEBUG)
    {
        printf("    > Checking for ethernet type... ");
    }
    int etherType = ntohs(e_hdr->ether_type);
    
    int sizeOfEhdr = sizeof(struct sr_ethernet_hdr);
    int sizeOfIP = sizeof(struct ip);
    int sizeOfAhdr = sizeof(struct sr_arphdr);
    int sizeOfEtherHost = sizeof(uint8_t) * ETHER_ADDR_LEN;
    
    // Check ether type
    switch(etherType)
    {
            
        /* --------------------------------------------------
         *  ARP
         * -------------------------------------------------- */
        case ETHERTYPE_ARP:
            
            if(DEBUG)
            {
                printf("ARP\n");
            }
            
            //printf("    - Size of e_hdr: %d\n", sizeOfEhdr);
            //printf("    - Size of a_hdr: %d\n", sizeOfAhdr);
            
            if(DEBUG)
            {
                printf("    > Checking for ARP type... ");
            }
            struct sr_arphdr * a_hdr = (struct sr_arphdr *)(packet + sizeOfEhdr);
            
            int arpRequet = ntohs(a_hdr->ar_op);
            
            //printf("    - ahdr: %d, %d, op: %d\n", a_hdr->ar_sip, a_hdr->ar_tip, arpRequet);
            
            
            /* ARP Request */
            if(arpRequet == ARP_REQUEST)
            {
                if(DEBUG)
                {
                    printf("REQUEST\n");
                }
                
                // TODO
                struct sr_ethernet_hdr * e_hdr_reply = (struct sr_ethernet_hdr *)malloc(sizeof(struct sr_ethernet_hdr));
                
                
                
                memcpy(e_hdr_reply->ether_dhost, e_hdr->ether_shost, sizeOfEtherHost);
                
                memcpy(e_hdr_reply->ether_shost, iface->addr, sizeOfEtherHost);
                e_hdr_reply->ether_type = e_hdr->ether_type;
                
                
                
                struct sr_arphdr * a_hdr_reply = (struct sr_arphdr *)malloc(sizeof(struct sr_arphdr));
                
                
                
                //printf("    - a_hdr_reply: op: %d\n", ntohs(a_hdr_reply->ar_op));
                
                memcpy(a_hdr_reply, a_hdr, sizeOfAhdr);
                
                a_hdr_reply->ar_op = htons(ARP_REPLY);//ARP_REQUEST);
                a_hdr_reply->ar_sip = a_hdr->ar_tip;
                a_hdr_reply->ar_tip = a_hdr->ar_sip;
                
                
                if(DEBUG)
                {
                    printf("    > Original source address: ");
                    DebugMAC(a_hdr_reply->ar_sha);
                    printf("\n");
                }
                //printf(" <<<%s>>>\n", a_hdr_reply->ar_sha);
                
                
                //memset(a_hdr_reply->ar_sha, '\0', MAC_SIZE);
                memcpy(a_hdr_reply->ar_sha, iface->addr, MAC_SIZE);
                
                //memset(a_hdr_reply->ar_tha, '\0', MAC_SIZE);
                memcpy(a_hdr_reply->ar_tha, a_hdr->ar_sha, MAC_SIZE);
                
                
                if(DEBUG)
                {
                    printf("    > Source ip: %d\n", ntohs(a_hdr_reply->ar_sip));
                    printf("    > iFace ip: %d\n", ntohs(iface->ip));
                    
                    printf("    > Source address: ");
                    DebugMAC(a_hdr_reply->ar_sha);
                    printf("\n");
                    //printf(" <<<%s>>>\n", a_hdr_reply->ar_sha);
                    
                    printf("    > iFace address: ");
                    DebugMAC(iface->addr);
                    printf("\n");
                    //printf(" <<<%s>>>\n", iface->addr);
                }
                
                
                //printf("    - a_hdr_reply: op: %d\n", ntohs(a_hdr_reply->ar_op));
                
                
                // Send a packet
                
                //len - sizeof(c_packet_ethernet_header) + sizeof(struct sr_ethernet_hdr)
                int length = sizeOfEhdr + sizeOfAhdr;
                uint8_t * aPacket = (uint8_t *)malloc(length);
                
                
                memcpy(aPacket, e_hdr_reply, sizeOfEhdr);
                //*aPacket = (*aPacket) << sizeOfAhdr;
                memcpy(aPacket + sizeOfEhdr, a_hdr_reply, sizeOfAhdr);
                //memcpy(aPacket, a_hdr_reply, sizeOfAhdr);
                
                //len - sizeOfEhdr + sizeof(struct sr_ethernet_hdr)
                sr_send_packet(sr, aPacket, length, iface->name);
                
                
            }
            
            
            
            /* ARP Reply */
            else if(arpRequet == ARP_REPLY)
            {
                if(DEBUG)
                {
                    printf("REPLY\n");
                }
                printf(">>> > We got the reply!!!\n");
                
                if(DEBUG)
                {
                    printf("    > Original source address: ");
                    DebugMAC(a_hdr->ar_sha);
                    printf("\n");
                    
                    printf("    > Source ip: %d\n", ntohs(a_hdr->ar_sip));
                    printf("    > iFace ip: %d\n", ntohs(iface->ip));
                    
                    printf("    > Source address: ");
                    DebugMAC(a_hdr->ar_sha);
                    printf("\n");
                    //printf(" <<<%s>>>\n", a_hdr_reply->ar_sha);
                    
                    printf("    > iFace address: ");
                    DebugMAC(iface->addr);
                    printf("\n");
                    //printf(" <<<%s>>>\n", iface->addr);
                }
                // TODO
                
                int xcode_find = list_find(ntohl(a_hdr->ar_sip));
                
                if(xcode_find == FOUND)
                {
                    if(DEBUG)
                    {
                        printf("    > Found\n");
                    }
                    // Do nothing
                }
                else if(xcode_find == NOT_FOUND)
                {
                    if(DEBUG)
                    {
                        printf("    > Not found\n");
                    }
                    // Insert into the front of the list if not fond
                    list_insert_front(ntohl(a_hdr->ar_sip), a_hdr->ar_sha, sr);
                    
                }
                else if(xcode_find == TIME_OUT)
                {
                    if(DEBUG)
                    {
                        printf("    > Time out\n");
                    }
                }
                else
                {
                    if(DEBUG)
                    {
                        printf("    > Unknown\n");
                    }
                    printf("ERROR: Unknown error occurred on finding linked-list\n");
                }
                
                
                if(DEBUG)
                {
                    print_list();
                }
            }
            
            
            
            /* ARP Unknown */
            else
            {
                if(DEBUG)
                {
                    printf("ERROR\n");
                }
                printf("ERROR: Unknow ARP response");
            }
            
            break;
            
            
            
            
            
            
            
            
            
            
            
            
            
        /* --------------------------------------------------
         *  IP
         * -------------------------------------------------- */
        case ETHERTYPE_IP:
            
            
            if(DEBUG)
            {
                printf("IP\n");
            }
            
            struct ip * ip_hdr = (struct ip *)(packet + sizeOfEhdr);
            
            if(DEBUG)
            {
                printf("    > Header length: %d\n", ip_hdr->ip_hl);
                printf("    > Version: %d\n", ip_hdr->ip_v);
                
                printf("    > Source address: %s\n", inet_ntoa(ip_hdr->ip_src));
                printf("    > Destination address: %s\n", inet_ntoa(ip_hdr->ip_dst));
                
                
                printf("    > Type of service: %d\n", ip_hdr->ip_tos);
                printf("    > Total length: %d\n", ip_hdr->ip_len);
                
                printf("    > Identification: %d\n", ip_hdr->ip_id);
                printf("    > Fragment offset field: %d\n", ip_hdr->ip_off);
                
                printf("    > Protocol: %d\n", ip_hdr->ip_p);
                printf("    > Checksum: %d\n", ip_hdr->ip_sum);
                
                printf("    > TTL: %d\n", ip_hdr->ip_ttl);
            }
            
            
            /* ICMP */
            if(ip_hdr->ip_p == IPPROTO_ICMP)
            {
                if(DEBUG)
                {
                    printf("    > Protocol: ICMP\n");
                }
                printf("Warning: ICMP received\n");
                
                break;
            }
            /* TCP */
            else if(ip_hdr->ip_p == TCP)
            {
                if(DEBUG)
                {
                    printf("    > Protocol: TCP\n");
                }
            }
            
        
            
            
            
            // Get check sum
            int check_sum_ip_header_length = ip_hdr->ip_hl * 2;
        
            uint16_t check_sum = ip_hdr->ip_sum;
            ip_hdr->ip_sum = 0;
            
            if(DEBUG)
            {
                printf("    > Checking check sum... ");
            }
            
            uint16_t check_sum_result = cksum((u_short *)ip_hdr, check_sum_ip_header_length);
        
            //printf("\n\n    - check_sum: %d\n", check_sum);
            //printf("    - check_sum_result_o: %d\n", check_sum_result);
            
            if(check_sum_result != check_sum)
            {
                if(DEBUG)
                {
                    printf("FAILED\n");
                }
                printf("ERROR: Check sum failed\n");
                
                break;  // Ignore the current packet
            }
            else
            {
                if(DEBUG)
                {
                    printf("OK\n");
                }
            }
            
            
            
            
            
            
            // Process the IP packet
            //struct sr_rt * rt_eth0;
            
            // Check destination ip
            int xcode_ck_dip = ck_destination_ip(iface, (uint32_t)ip_hdr->ip_dst.s_addr);
            
            // Packet is at its destination
            // Processing
            if(xcode_ck_dip == VALID)
            {
                if(DEBUG)
                {
                    printf("    > Destination address reached\n");
                    printf("    > Process the packet\n");
                    
                }
                
                
                
                int ip_packet_size = sizeOfIP + sizeOfEhdr;
                uint8_t * aPacket = malloc(ip_packet_size);
                
                memcpy(aPacket, packet, ip_packet_size);
                
                
                
                struct sr_ethernet_hdr *aPacket_ehdr = (struct sr_ethernet_hdr *)aPacket;
                
                uint8_t * ether_shost_tmp = malloc(sizeOfEtherHost);
                
                memcpy(ether_shost_tmp, aPacket_ehdr->ether_shost, ETHER_ADDR_LEN);
                
                memcpy(aPacket_ehdr->ether_shost, aPacket_ehdr->ether_dhost, ETHER_ADDR_LEN);
                
                memcpy(aPacket_ehdr->ether_dhost, ether_shost_tmp, ETHER_ADDR_LEN);
                
                //memcpy(dest_ethr, aPacket_ehdr, ETHER_ADDR_LEN);
                //memcpy(dest_ethr, aPacket_ehdr, ETHER_ADDR_LEN);
                
                
                struct ip *aPacket_ehdr_ip = (struct ip *)(aPacket + sizeOfEhdr);
                
                uint32_t ip_src_tmp = aPacket_ehdr_ip->ip_src.s_addr;
                
                aPacket_ehdr_ip->ip_src.s_addr = aPacket_ehdr_ip->ip_dst.s_addr;
                
                aPacket_ehdr_ip->ip_dst.s_addr = ip_src_tmp;
                
                
                // Updated the checksum
                aPacket_ehdr_ip->ip_sum = 0;
                aPacket_ehdr_ip->ip_sum = cksum((u_short *)aPacket_ehdr_ip, check_sum_ip_header_length);
                
                
                if(DEBUG)
                {
                    printf("    > Sending the new packet\n");
                }
                sr_send_packet(sr, aPacket, len, iface->name);
                
                
            }
            
            
            
            
            // Packet is NOT at its destination
            // Forwarding
            else if(xcode_ck_dip == INVALID)
            {
                if(DEBUG)
                {
                    printf("    > Destination address is NOT the same as the senders address\n");
                    printf("    > Forwarding started\n");
                }
                
                
                //struct in_add target = ip_hdr->ip_dst;
                
                
                
                if(DEBUG)
                {
                    printf("    > Checking TTL... \n");
                }
                uint8_t TTL = ip_hdr->ip_ttl;
                
                // Decrementing TTL
                if(TTL > 0)
                {
                    if(DEBUG)
                    {
                        printf("OK\n");
                    }
                    
                    TTL--;
                    
                    ip_hdr->ip_ttl = TTL;
                    
                    ip_hdr->ip_sum = cksum((u_short *)ip_hdr, check_sum_ip_header_length);
                    
                    printf("    > Checksum for new ttl is: %d \n", ip_hdr->ip_sum);
                    
                    if(DEBUG)
                    {
                        printf("    > TTL is now decrememented and is now: %d\n", ip_hdr->ip_ttl);
                    }
                    
                    
                    
                }
                else
                {
                    if(DEBUG)
                    {
                        printf("FAILED\n");
                    }
                    printf("ERROR: TTL %d is <= 0\n", TTL);
                    
                    break;  // Ignore the current packet
                }
                
                struct in_addr dst_addr = ip_hdr->ip_dst;
                
                
                
                // Find the next hop
                uint32_t tmp_mask = SUBMASK;
                
                struct sr_rt * rt_table = sr->routing_table;
                
                while(rt_table)
                {
                    
                    
                    struct in_addr addr_msk = rt_table->mask;
                    struct in_addr current_dst_addr = rt_table->mask;
                    
                    uint32_t converted_add = dst_addr.s_addr & addr_msk.s_addr;
                    
                    if(tmp_mask < addr_msk.s_addr && converted_add == current_dst_addr.s_addr)
                    {
                        tmp_mask = addr_msk.s_addr;
                    }
                    else
                    {
                        
                    }
                    
                    rt_table = rt_table->next;
                }
                
                list_insert_front(ntohl(tmp_mask), 0, sr);
            }
            // Should not be seeing this forever
            else
            {
                printf("ERROR: Unknown error occurred while checking the destination IP of the current packet\n");
            }
            
            
            
            
            
            
            
            
            /*
            if(DEBUG)
            {
                printf("    > Checking TTL... \n");
            }
            uint8_t TTL = ip_hdr->ip_ttl;
            
            if(TTL > 0)
            {
                TTL--;
                
                ip_hdr->ip_ttl = TTL;
                
                ip_hdr->ip_sum = cksum((u_short *)ip_hdr, check_sum_ip_header_length);

                printf("    > Checksum for new ttl is: %d \n", ip_hdr->ip_sum);

                if(DEBUG)
                {
                    printf("    > TTL is now decrememented and is now: %d\n", ip_hdr->ip_ttl);
                    printf("OK\n");
                }
            }
            else
            {
                if(DEBUG)
                {
                    printf("FAILED\n");
                }
                printf("ERROR: TTL %d is <= 0\n", TTL);
                
                break;  // Ignore the current packet
            }
            */
            
            /*
            //trying to forward packet

            struct ip * temp_ip = (struct ip *)(packet + sizeOfEhdr);
            struct in_addr  dest = ip_hdr->ip_dst;

            check TTL (already should be good)



            
            
            */



                        
            
            
            
            //struct sr_arphdr * a_hdr = (struct sr_arphdr *)(packet + sizeOfEhdr);
            break;
            
            
            
            
        /* --------------------------------------------------
         *  ICMP
         * -------------------------------------------------- */
        case IPPROTO_ICMP:
            if(DEBUG)
            {
                printf("ICMP\n");
            }
            break;
            
            
            
            
        /* --------------------------------------------------
         *  Unknown
         * -------------------------------------------------- */
        default:
            
            printf("ERROR: Unknow (%d) etherType\n", etherType);
            
            break;
        
    }
    
    // Send a packet
    //sr_send_packet(sr, packet, len, interface);
    
    
}/* end sr_ForwardPacket */


/*
add_rt_entry{
    

}


*/

/* --------------------------------------------------
 *  cksum:
 * -------------------------------------------------- */
static u_short
cksum(u_short * buf, int count)
{
    register u_long sum = 0;
    
    while(count--)
    {
        sum += *buf++;
        if(sum & 0xFFFF0000)
        {
            /* carry occurred, so wrap around */
            sum &= 0xFFFF;
            sum++;
        }
        
    }
    return ~(sum & 0xFFFF);
}




/* --------------------------------------------------
 *  ck_destination_ip:
 * --------------------------------------------------
 *  return:
 *      VALID       - packet is at its destination
 *      INVALID     - exit with no error
 * -------------------------------------------------- */
static int ck_destination_ip(struct sr_if * iface_list, uint32_t ip)
{
    if(DEBUG)
    {
        printf("\n*** @ ck_destination_ip: Started\n");
    }
    struct sr_if * head = iface_list;
    
    if(DEBUG)
    {
        printf("    > Checking destination ip... ");
    }
    //printf(">>> ip: %d\n", ip);
    
    while(head)
    {
        uint32_t current_ip = head->ip;
        
        //printf(">>> current_ip: %d\n", current_ip);
        if(current_ip != ip)
        {
            head = head->next;
        }
        else
        {
            if(DEBUG)
            {
                printf("OK\n");
            }
            return VALID;
        }
        
    }
    if(DEBUG)
    {
        printf("OK\n");
    }
    return INVALID;
}






/* --------------------------------------------------
 *  destroy
 * --------------------------------------------------
 *  return:
 *      < 0         - something wrong happened
 *      1           - exit with no error
 * -------------------------------------------------- */
/*
static int destroy()
{
    int count = 0;
    
    if(ARP_Cache)
    {
        free(ARP_Cache);
    }
    else
    {
        count--;
    }
    
    // TODO
    // More to be destoried
    if(count < 0)
    {
        return count;
    }
    
    return VALID;
}

*/









/* --------------------------------------------------
 *  get_time
 * --------------------------------------------------
 *  return:
 *      current time in seconds
 * -------------------------------------------------- */
static int get_time()
{
    time_t seconds;
    seconds = time(NULL);
    
    return seconds + 0;
}





















/*---------------------------------------------------------------------
 * Linked-list Instructions:
 *---------------------------------------------------------------------*/

/* --------------------------------------------------
 *  print_list
 * -------------------------------------------------- */
static void print_list()
{
    if(DEBUG)
    {
        printf("\n*** @ print_list: Started\n");
    }
    ARP_Packet * head = ARP_Cache;
    
    i = 0;
    while(head)
    {
        
        printf("ARP - %d\n", i);
        printf("Type Name: %s\n", head->type);
        printf("ID: %d\n", head->id);
        printf("Reference: %d\n", head->ref);
        
        
        printf("MAC: ");
        DebugMAC(head->ar_mac);
        //printf("\nTarget MAC: ");
        //DebugMAC(head->ar_tha);
        
        
        //printf("\nSender IP: %d\n", head->ar_sip);
        printf("\nIP: %d\n", head->ar_ip);
        
        head = head->next;
        i++;
    }
}


/* --------------------------------------------------
 *  list_insert_front
 * -------------------------------------------------- */
static void list_insert_front(uint32_t ip, unsigned char * mac, struct sr_instance *sr)
{
    ARP_Packet * a_packet = (ARP_Packet *)malloc(sizeof(ARP_Packet));
    
    
    a_packet->ar_ip = ip;
    a_packet->sr = sr;
    memcpy(a_packet->ar_mac, mac, MAC_SIZE);
    
    int time = get_time();
    a_packet->time_received = time;
    
    a_packet->next = NULL;
    
    if(ARP_Cache)
    {
        a_packet->next = ARP_Cache;
        ARP_Cache = a_packet;
    }
    else
    {
        ARP_Cache = a_packet;
    }
}



/* --------------------------------------------------
 *  list_insert_rear
 * -------------------------------------------------- */
/*
static void list_insert_rear(uint32_t ip, unsigned char * mac, struct sr_instance *sr)
{
    ARP_Packet * a_packet = (ARP_Packet *)malloc(sizeof(ARP_Packet));
    
    
    a_packet->ar_ip = ip;
    a_packet->sr = sr;
    memcpy(a_packet->ar_mac, mac, MAC_SIZE);
    
    int time = get_time();
    a_packet->time_received = time;
    
    a_packet->next = NULL;
    
    
    if(ARP_Cache == NULL)
    {
        ARP_Cache = a_packet;
        return;
    }
    
    
    ARP_Packet * head = ARP_Cache;
    
    while(head->next)
    {
        head = head->next;
    }
    
    head->next = a_packet;
}

*/




/* --------------------------------------------------
 *  list_find
 * --------------------------------------------------
 *  return:
 *      FOUND       - found
 *      NOT_FOUND   - not found
 *      TIME_OUT    - time out
 *      INVALID     - error occurred
 * -------------------------------------------------- */
static int list_find(uint32_t ip)
{
    if(DEBUG)
    {
        printf("\n*** @ list_find: Started\n");
    }
    
    ARP_Packet * head = ARP_Cache;
    
    while(head)
    {
        
        
        // Found it
        if(head->ar_ip == ip)
        {
            // Updated mac_found
            memcpy(mac_found, head->ar_mac, MAC_SIZE);
        }
        
        // Check time
        int seconds = get_time();
        
        int time_diff = seconds - head->time_received;
        
        if(time_diff > SESSION_TIME)
        {
            return TIME_OUT;
        }
        else
        {
            return FOUND;
        }
        
        head = head->next;
    }
    
    return NOT_FOUND;
}
