Hercy (Yunhao Zhang)
	yunhaozhang@email.arizona.edu

Jeremy Mark Norris
	jnorris4118@email.arizona.edu

We have:
ARP requests and replies implemented and worked.
ARP Cache implemented and worked.
ARP time out(15s) check and related link-list instructions.
IP Check destination is working.
Checksum and TTL check and decrement working.
Find out the next hop implemented.
Also Cached into cache for eating to send to the next hop.